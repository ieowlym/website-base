function createCard(url, name, price) {
    // Create a new div element with the "card" class
    var card = document.createElement('div');
    card.className = 'card';

    // Create an img element with the provided URL
    var img = document.createElement('img');
    img.src = url;
    img.alt = '';

    // Create a paragraph element for the name with the "name" class
    var nameParagraph = document.createElement('p');
    nameParagraph.className = 'name';
    nameParagraph.textContent = name;

    // Create a paragraph element for the price with the "price" class
    var priceParagraph = document.createElement('p');
    priceParagraph.className = 'price';
    priceParagraph.innerHTML = price + ' <i class="fa-solid fa-tenge-sign"></i>';

    // Create a button element with the text "Купить"
    var buyButton = document.createElement('button');
    buyButton.textContent = 'Купить';

    // Append the created elements to the card div
    card.appendChild(img);
    card.appendChild(nameParagraph);
    card.appendChild(priceParagraph);
    card.appendChild(buyButton);

    // Get the container element by ID
    var container = document.getElementById('products');

    // If the container exists, append the card to it
    if (container) {
        container.appendChild(card);
    } else {
        console.error('Container with ID ' + containerId + ' not found.');
    }
}

createCard(
    'https://klike.net/uploads/posts/2023-01/1675081401_3-42.jpg',
    'Букет 21шт роза', 
    '13 500'
);